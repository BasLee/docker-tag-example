#!/bin/sh

set -x

echo $DOCKER_TAG
echo $SOURCE_COMMIT
echo $IMAGE_NAME

rm -rf ./target/
mkdir ./target/

envsubst < index.json > target/index.json
cat target/index.json

tail -f /dev/null
